local Shared = require"ecs/shared/shared"
local Camera = require"library/STALKER-X/Camera"

return class("Camera",Shared){
    _create = function()
        return Camera()
    end
}()