local shared = class("Shared"){}

function shared:__init__()
    box_core.set_shared(self.__name__,self:_create())
end

function shared:_create()
    return {}
end

return shared