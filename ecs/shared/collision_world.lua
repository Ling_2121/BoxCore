local Shared = require"ecs/shared/shared"
local bump3d = require"library/bump-3dpd"

return class("CollisionWorld",Shared){
    _create = function()
        return bump3d.newWorld(256)
    end
}()