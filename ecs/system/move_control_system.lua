local System = require"ecs/system/system"

local move_control_system = class("MoveControlSystem",System){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = {
            "MoveControllerComponent",
            "VelocityComponent",
            "DirectionComponent",
            "StateComponent",
        };
    },
}

local UP_ROT = math.rad(-180)
local DOWN_ROT = math.rad(0)
local LEFT_ROT = math.rad(90)
local RIGHT_ROT = math.rad(-90)

function move_control_system:process(entity,dt)
    -- local vx,vy = 0,0
    -- if love.keyboard.isDown('w') then
    --     vy = -entity.move_speed
    -- end
    -- if love.keyboard.isDown('s') then
    --     vy = entity.move_speed
    -- end
    -- if love.keyboard.isDown('a') then
    --     vx = -entity.move_speed
    -- end
    -- if love.keyboard.isDown('d') then
    --     vx = entity.move_speed
    -- end

    if love.keyboard.isDown("w") then
        entity.velocity_x = entity.move_speed * entity.direction_x
        entity.velocity_y = entity.move_speed * entity.direction_y
        entity.state = "move"
    else
        entity.velocity_x = 0 
        entity.velocity_y = 0 
        entity.state = "idle"
    end
end

return move_control_system()