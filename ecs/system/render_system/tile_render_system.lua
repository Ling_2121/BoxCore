local BaseRenderSystem = require"ecs/system/render_system/base_render_system"

local tile_render_system = class("TileRenderSystem",BaseRenderSystem){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = function(self,entity)
            return entity["RendererComponent"] and entity.render_type == RENDER_TYPE_TILE
        end,
        render_priority = -1;
    },
}

function tile_render_system:process(renderer,dt)
    local z_index = math.ceil(renderer.z_index)
    local up_z_index = renderer._up_z_index or math.huge
    if up_z_index ~= z_index then
        local old_seg = self.zlist:get_segment(up_z_index)
        local new_seg = self.zlist:get_segment(z_index)
        old_seg:remove(renderer)
        new_seg:add(z_index,renderer)
        renderer._up_z_index = z_index
    end
end

function tile_render_system:draw()
    local camera = self.camera
    camera:attach()
    self.zlist:foreach(function(renderer)
        renderer._render(renderer.bind_entity,camera,renderer.bind_entity.render_items)
    end)
    camera:detach()
end

return tile_render_system()