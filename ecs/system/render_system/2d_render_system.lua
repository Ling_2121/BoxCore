local BaseRenderSystem = require"ecs/system/render_system/base_render_system"

local render_system_2d = class("2DRenderSystem",BaseRenderSystem){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = function(self,entity)
            return entity["RendererComponent"] and entity.render_type == RENDER_TYPE_2D
        end,
        render_priority = 1;
    },
}

function render_system_2d:draw()
    local camera = self.camera
    camera:attach()
    self.zlist:foreach(function(renderer)
        renderer._render(renderer.bind_entity,camera,renderer.bind_entity.render_items)
    end)
    camera:detach()
end

return render_system_2d()