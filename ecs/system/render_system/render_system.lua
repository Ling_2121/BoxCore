local System = require"ecs/system/system"

local render_system = class("RenderSystem",System){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = {"RenderComponent"};
    },

    
}

function render_system:_init()
    self.camera = box_core.get_shared("Camera")
end

function render_system:onAdd(entity)
    for k,item in pairs(entity.render_items) do
        if item:is("RenderProcessItem") then
            table.insert(entity._process_ritems,item)
        end
    end
    for i,renderer in ipairs(entity.renderers) do
        renderer.bind_entity = entity
        tiny.addEntity(box_core.world,renderer)
    end
end

function render_system:onRemove(entity)
    for i,renderer in ipairs(entity.renderers) do
        tiny.removeEntity(box_core.world,renderer)
    end
end

function render_system:postProcess(dt)
    self.camera:update(dt)
end

function render_system:process(entity,dt)
    for i,item in ipairs(entity._process_ritems) do
        item:process(dt)
    end
end

return render_system()