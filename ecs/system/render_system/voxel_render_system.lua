local BaseRenderSystem = require"ecs/system/render_system/base_render_system"

local voxel_render_system = class("VoxelRenderSystem",BaseRenderSystem){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = function(self,entity)
            return entity["RendererComponent"] and entity.render_type == RENDER_TYPE_VOXEL
        end,
        render_priority = 0;
    },
}

function voxel_render_system:preProcess()
    self.zlist:clear()
end

function voxel_render_system:process(renderer)
    local bind_entity = renderer.bind_entity
    local x,zi = self.camera:toCameraCoords(bind_entity.x,bind_entity.y)
    renderer.z_index = math.ceil(zi)
    self.zlist:add(renderer.z_index,renderer)
end

function voxel_render_system:draw()
    local camera = self.camera

    self.zlist:foreach(function(renderer)
        renderer._render(renderer.bind_entity,camera,renderer.bind_entity.render_items)
    end)

    camera._cam_up_r = camera.rotation
    camera._cam_up_s = camera.scale
end

return voxel_render_system()

