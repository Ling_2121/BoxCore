local BaseRenderSystem = require"ecs/system/render_system/base_render_system"

local screen_render_system = class("ScreenRenderSystem",BaseRenderSystem){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = function(self,entity)
            return entity["RendererComponent"] and entity.render_type == RENDER_TYPE_SCREEN
        end,
        render_priority = 2;
    },
}

function screen_render_system:draw()
    self.zlist:foreach(function(renderer)
        renderer._render(renderer.bind_entity,self.camera,renderer.bind_entity.render_items)
    end)
end

return screen_render_system()