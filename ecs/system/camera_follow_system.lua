local System = require"ecs/system/system"

local camera_follow_system = class("CameraFollowSystem",System){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = {
            "CameraFollowComponent",
            "PositionComponent"
        };
    },

    camera = nil;
}

function camera_follow_system:_init()
    self.camera = box_core.get_shared("Camera")
end

function camera_follow_system:process(entity)
    self.camera:follow(entity.x,entity.y)
end

return camera_follow_system()