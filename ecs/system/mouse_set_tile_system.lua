local System = require"ecs/system/system"

local mouse_set_tile_system =  class("MouseSetTileSystem",System){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = {};
    },

    camera = nil;
    sandbox_area_utils = nil;
}

function mouse_set_tile_system:_init(dt)
    self.camera = box_core.get_shared("Camera")
    self.sandbox_area_utils = box_core.get_utils("SandboxAreaUtils")

    self.tileindex = self.sandbox_area_utils.to_tileindex(0,1)
    box_core.callback:register("mousepressed",self,"mousepressed")
    box_core.callback:register("draw",self,"draw")
end

function mouse_set_tile_system:mousepressed(mx,my)
    mx,my = self.camera:getMousePosition(mx,my)
    self.sandbox_area_utils.set_tile(mx,my,self.tileindex)
end

function mouse_set_tile_system:draw()
    -- local x,y = love.mouse.getPosition()
    -- mx,my = self.camera:toCameraCoords(x,y)
    self.camera:attach()
    love.graphics.rectangle("line",-10,-10,20,20)
    love.graphics.pop()
end

return mouse_set_tile_system()