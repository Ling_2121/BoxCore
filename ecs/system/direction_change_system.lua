local System = require"ecs/system/system"
local vector = require"library.vector-light"

local direction_change_system = class("DirectionChangeSystem",System){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = {
            "DirectionComponent",
            "PositionComponent"
        };
    },

    camera = nil;
}

local function get_rot(x1,y1,x2,y2)
    if x1 == x2 and y1 == y2 then return 0 end 
	local angle = -math.atan((x2-x1)/(y2-y1))
	if y1-y2 < 0 then angle = angle+math.pi end
	if angle < 0 then angle = angle+2*math.pi end
	return angle
end


function direction_change_system:_init()
    self.camera = box_core.get_shared("Camera")
end

function direction_change_system:process(entity)
    local mx,my = self.camera:getMousePosition()
    entity.rotate = get_rot(mx,my,entity.x,entity.y)
    entity.direction_x,entity.direction_y = vector.normalize((mx - entity.x),(my - entity.y))
end


return direction_change_system()