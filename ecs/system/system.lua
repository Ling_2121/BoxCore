
local system = class("System"){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = false;
        filter = {}
    }
}

function system:__init__()
    local system_type = self.system_config.type
    local match_system = {
        [SYSYTEM_TYPE_SYSTEM] = tiny.system,
        [SYSYTEM_TYPE_PROCESS] = tiny.processingSystem,
        [SYSYTEM_TYPE_SORT] = tiny.sortedSystem,
        [SYSYTEM_TYPE_SORT_PROCESS] = tiny.sortedProcessingSystem,
    } 

    local system = match_system[system_type](self)
    local filter = self.system_config.filter
    box_core.systems[self.__name__] = system

    if type(filter) == "table" then
        system.filter = tiny.requireAll(unpack(self.system_config.filter))
    else
        system.filter = filter
    end
    if self.system_config.is_default_add then
        tiny.addSystem(box_core.world,system)
    end
    self:_init()
end

function system:_init()
    
end

return system