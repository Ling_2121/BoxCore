local System = require"ecs/system/system"

local move_system = class("MoveSystem",System){
    system_config = {
        type = SYSYTEM_TYPE_PROCESS;
        is_default_add = true;
        filter = {
            "PositionComponent",
            "VelocityComponent",
            tiny.requireAny("SizeComponent"),
        };
    },
    collision_world = nil;
}

function move_system:_init()
    self.collision_world = box_core.get_shared("CollisionWorld")
end

function move_system:process(entity,dt)
    if entity.SizeComponent then
        self.collision_world:move(
            entity,
            entity.x + entity.velocity_x * dt,
            entity.y + entity.velocity_y * dt,
            entity.z + entity.velocity_z * dt
        )
    else
        entity.x = entity.x + entity.velocity_x * dt
        entity.y = entity.y + entity.velocity_y * dt
        entity.z = entity.z + entity.velocity_z * dt
    end

    entity.z_index = entity.y
end

return move_system()