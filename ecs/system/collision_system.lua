local System = require"ecs/system/system"

local collision_system = class("CollisionSystem",System){
    system_config = {
        type = SYSYTEM_TYPE_SYSTEM;
        is_default_add = true;
        filter = {
            "PositionComponent",
            "SizeComponent",
        };
    },

    collision_world = nil;
}

function collision_system:_init()
    self.collision_world = box_core.get_shared("CollisionWorld")
end

function collision_system:onAdd(entity)
    self.collision_world:add(entity)
end

function collision_system:onRemove(entity)
    self.collision_world:remove(entity)
end


return collision_system()