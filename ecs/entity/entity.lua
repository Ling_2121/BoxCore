local entity = class("Entity"){
    --[[
        "组件名",
        {"属性",值}
    --]]
}

local function copy_value(value)
    if type(value) ~= "table" then
        return value
    end
    local cpt = {}
    for k,v in pairs(value) do
        cpt[copy_value(k)] = copy_value(v)
    end

    return setmetatable(cpt,value)
end

function entity:__init__()
    box_core.entitys[self.__name__] = self
end

function entity:_init(entity,init_values)

end

function entity:create(init_value)
    init_value = init_value or {}
    local e = {}
    for i,c_name in ipairs(self) do
        local c = box_core:get_component(c_name)
        if c then
            e[c.__name__] = true;
            for i,kv in ipairs(c) do
                e[kv[1]] = copy_value(kv[2])
                if init_value[kv[1]] ~= nil then
                    e[kv[1]] = init_value[kv[1]]
                end
            end
            c:_init(e)
        else
            e[c_name[1]] = copy_value(c_name[2])
        end
    end

    self:_init(e,init_value)
    return e
end

return entity()