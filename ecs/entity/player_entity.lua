local Entity = require"ecs/entity/entity"
local RednererEntity = require"ecs/entity/renderer_entity"

local player_entity = class("PlayerEntity",Entity){
    "PositionComponent",
    "SizeComponent",
    "RenderComponent",
    "VelocityComponent",
    "MoveControllerComponent",
    "DirectionComponent",
    "CameraFollowComponent",
    "StateComponent",
}

function player_entity:_init(entity,init_values)
    local player_model = box_core.assets.create_model("assets/model/human/idle/idle_1.vox")
    local RenderUtils = box_core.get_utils("RenderUtils")



    entity.render_items = {
        idle_model = player_model;
        move_anim = RenderUtils.item.animation(ANIMATION_TYPE_VOXEL,{
            play_speed = 0.2;
            frames = {
                box_core.assets.create_model("assets/model/human/move/move_1.vox"),
                box_core.assets.create_model("assets/model/human/move/move_2.vox")
            }
        })
    }

    entity.renderers = {
        RednererEntity:create{
            render_type = RENDER_TYPE_VOXEL,
            _render = function(self,camera,render_items)
                if self.state == "idle" then
                    render_items.idle_model:draw(self.x,self.y,self.z,self.rotate)
                end
                if self.state == "move" then
                    render_items.move_anim:draw(self.x,self.y,self.z,self.rotate)
                end
            end
        }
    }
end

return player_entity()