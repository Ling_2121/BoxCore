local Entity = require"ecs/entity/entity"
local RednererEntity = require"ecs/entity/renderer_entity"

local sandbox_area_entity =  class("SandboxAreaEntity",Entity){
    "RenderComponent";
    "SandboxAreaComponent";
}

function sandbox_area_entity:_init(entity)
    entity.z_index = 0
    entity.renderers = {
        RednererEntity:create{
            render_type = RENDER_TYPE_TILE,
            _render = function(self,camera)
                love.graphics.draw(self.canvas,self.wx,self.wy)
                love.graphics.rectangle("line",self.wx,self.wy,SANDBOX_AREA_PSIZE,SANDBOX_AREA_PSIZE)
            end
        }
        
    }
end

return sandbox_area_entity();