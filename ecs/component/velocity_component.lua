local Component = require"ecs/component/component"

return class("VelocityComponent",Component){
    {"move_speed",100};
    {"velocity_x",0};
    {"velocity_y",0};
    {"velocity_z",0};
}()