local Component = require"ecs/component/component"

return class("DirectionComponent",Component){
    {"direction_x",0},
    {"direction_y",0},
    {"rotate",0},
}()