local Component = require"ecs/component/component"

return class("PositionComponent",Component){
    {"x",0};
    {"y",0};
    {"z",0};
}()