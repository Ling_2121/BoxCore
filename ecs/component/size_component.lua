local Component = require"ecs/component/component"


--[[
      z      y
      l    depth
heightl  /
      l/
      .------- x
        width
--]]
return class("SizeComponent",Component){
    {"width",16};
    {"height",16};
    {"depth",16};
}()