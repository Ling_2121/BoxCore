local Component = require"ecs/component/component"

return class("RenderComponent",Component){
    {"z_index",0};
    {"render_items",{}},--要绘制渲染的项目（一个存储区，项目需要更新的话会自动更新）
    {"_process_ritems",{}},
    {"renderers",{}};--渲染器组
}()