local Component = require"ecs/component/component"

return class("RendererComponent",Component){
    {"bind_entity",0};
    {"z_index",0};
    {"inherit_z_index",true};
    {"_render",function(entity,render_items) end};
    {"render_type",RENDER_TYPE_2D}
}()