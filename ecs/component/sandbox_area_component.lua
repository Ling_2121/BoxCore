local Component = require"ecs/component/component"


local area =  class("SandboxAreaComponent",Component){
    {"x",0};
    {"y",0};
    {"wx",0};
    {"wy",0};
    {"grids",{}};
    {"objects",{}};
    {"status",AREA_STATUS_STOP};
    {"canvas",0};
}

function area:_init(entity)
    entity.canvas = love.graphics.newCanvas(SANDBOX_AREA_PSIZE,SANDBOX_AREA_PSIZE)
end

return area()