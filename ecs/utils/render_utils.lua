local Utils = require"ecs.utils.utils"
local Rectangle = require"module/render/render_objects/rectangle"
local Circle = require"module/render/render_objects/circle"
local Texture = require"module/render/render_objects/texture"
local Voxel = require"module/render/render_objects/voxel/model"
local TextureAnimation = require"module/render/render_objects/animation/texture_animation"
local VoxelAnimation = require"module/render/render_objects/animation/voxel_animation"

local render_utils = class("RenderUtils",Utils){
    item = {}
}

function render_utils.item.rectangle(fill_mode,width,height)
    return Rectangle(fill_mode,width,height)
end

function render_utils.item.circle(fill_mode,radius)
    return Circle(fill_mode,radius)
end

function render_utils.item.texture(texture_path)
    return Texture(texture_path)
end

function render_utils.item.voxel(voxel_model)
    return Voxel(voxel_model)
end


--[[
    animation_config {
        *play_speed = 0.5;--播放速度
        *start_frame = 1;--起始帧
        *playing = true;--是否播放
        frames = {
            对应动画的渲染项
            ...
        }
    }
--]]
local anim_types = {
    [ANIMATION_TYPE_TEXTURE] = TextureAnimation,
    [ANIMATION_TYPE_VOXEL] = VoxelAnimation,
}

function render_utils.item.animation(type,animation_config)
    local anim = anim_types[type]()
    anim.play_speed = animation_config.play_speed or 0.5
    anim.frame = animation_config.start_frame or 1
    anim.playing = animation_config.playing or true
    anim._frame_count = #animation_config.frames
    anim.frames = animation_config.frames
    return anim
end

return render_utils()