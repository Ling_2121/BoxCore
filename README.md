## BoxCore

>   Love2D游戏开发ECS框架，用于2D体素游戏的开发

## 框架结构

| 文件夹  | 功能         |
| ------- | ------------ |
| assets  | 资源目录     |
| base    | 基础拓展     |
| docs    | 文档         |
| ecs     | ECS对象目录  |
| library | 第三方库目录 |
| module  | 程序模块     |
| test    | 测试         |

## 框架安装

**需要Love2D 0.11以上版本**

**拷贝一份即可，不用安装，只需love2d引擎即可**

## class的用法

本框架为lua加入了一个简易的面对对象系统，可以方便的创建对象，通过class函数

类的创建

```lua
-- 一个类的创建形式如下
-- local TestClass = class("类名",继承列表){
--        数据成员
-- }
-- function TestClas:__init__()
--        这是构造函数
-- end

local Vector1D = class("Vector1D"){
    x = 0
}

local Vector2D = class("Vector2D",Vector1D){
    y = 0
}
function Vector2D:__init__(x,y)
    self.x = x or 0
    self.y = y or 0
end
```

类的实例化

```lua
local vec1 = Vector2D(100,200)
print(vec1.x,vec2.y)

--> 100    200
```

## ECS结构

>   ​	无论什么类型的ecs对象，类名都是他的定义名称

#### ecs目录结构为

-   ecs
    -   entity  实体
    -   component    组件
    -   system    系统
    -   shared    数据共享区
    -   utils    实用程序集

#### **Entity**

>   ECS系统中的实体，一种数据表示的对象，区别于OOP，它只是包含数据，不包含操作。由组件组成。

实体存放于 <u>ecs/entity</u> 目录及其子目录

```lua
--player_entity.lua
local Entity = require"ecs/entity/entity"

--实体要继承Entity类。PlayerEntity为实体名称
local PlayerEntity  = class("PlayerEntity",Entity){
    --这是是实体需求组件的列表，填写组件类名称即可
    "PositionComponent",
    "RenderComponent"
}

--返回实体的函数形式调用则默认加入到系统中
return PlayerEntity()
```



#### Component

>   ECS系统中的组件，在此框架中作为构造器。

组件存放于 <u>ecs/component</u> 目录及其子目录

```lua
local Component = require"ecs/component/component"

local PositionComponent = class("PositionComponent",Component){
    --下面两行为组件的数据定义
    --形式为 : {"组件名称",组件数据}
    {"x",0},
    {"y",0},
}

return PositionComponent()
```

#### System

>   ECS系统中的系统，只是表示逻辑，不包含数据，会对拥有指定组件的实体进行更新和处理

```lua
local System = require"ecs/system/system"

local MoveSystem = class("MoveSystem",System){
    --系统配置
    system_config = {
        type = SYSYTEM_TYPE_PROCESS,
        is_default_add = true,
        filter = {"PositionComponent"},
    }
}
```

**system_config.type : [SYSYTEM_TYPE]**

| 类型名                    | 说明         |
| ------------------------- | ------------ |
| SYSYTEM_TYPE_SYSTEM       | 普通系统     |
| SYSYTEM_TYPE_PROCESS      | 普通处理系统 |
| SYSYTEM_TYPE_SORT         | 排序系统     |
| SYSYTEM_TYPE_SORT_PROCESS | 排序处理系统 |

**system_config.is_default_add : [true | false]**   

<u>为true时默认加入到时间</u>

**system_config.filter : [table |  function]**

实体过滤器，为<u>table</u>时配置需求的组件名称即可

```lua
--表示有 PositionComponent 和 VelocityComponent的对象才会加入到系统中
filter = {
    "PositionComponent",
    "VelocityComponent",
}
```

为function时

```lua
--返回true时表示entity通过过滤
filter = function(self,entity)
	return entity.PositionComponent and entity.VelocityComponent
end
```



ecs基于tiny-ecs库的实现，过滤器的具体内容请看 [这里](http://bakpakin.github.io/tiny-ecs/doc/#Filter_functions)，系统的回调及其数据请看[这里](http://bakpakin.github.io/tiny-ecs/doc/#System_functions)

#### Shared

>   这是存放共享数据的地方，系统和系统间不能直接通信，但可以通过共享区进行间接的通信

```lua
local Shared = require"ecs/shared/shared"
local CameraLibrary = require"library/camera"

--共享类继承自Shared
local Camera = class("Camera",Shared){
    --重载_create函数来反馈需要加入共享区的对象，共享类的类名对应在共享区中的类名
    _create = function()
        return CameraLibrary()
    end
}

return Camera()
```

获取共享数据的方式

```lua
local camera = box_core.get_shared("Camera")
```

#### Utils

>   这是用来操作或者设置的程序集

```lua
local Utils = require"ecs/utils/utils"

local RenderUtils = class("RenderUtils",Utils){}

function RenderUtils.draw(object)
	-- ...code...
end

function RenderUtils.draw_rectangle(fill_mode,x,y,w,h)
	-- ...code...
end

return RenderUtils()
```

获取程序集的方式

```lua
local RenderUtils = box_core.get_utils("RenderUtils")
```