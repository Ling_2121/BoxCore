local Node = require"module.scene.node"
local RanderNode = require"module.scene.render_node"
local Entity = require"module.ecc.entity"
local Component = require"module.ecc.component"
local Controller = require"module.ecc.controller"

local voxel = require"module.render.voxel.voxel"
local block_model_data = voxel.newModelData("assets/model/block.vox")
local block_model =voxel.newModel(block_model_data)

local player_model_data = voxel.newModelData("assets/model/chr_knight.vox")
local player_block_model = voxel.newModel(player_model_data)

local scene = class("Scene",Node){}

local BLOCK_COUNT = 1000

local block = class("TestObject",RanderNode){
    x = 0;
    y = 0;
    z = 0;
}

function block:_render()
    box_core.reader:render_model(block_model,self.x,self.y,self.z,0,2,2)
end

local PositionComponent = class("PositionComponent",Component){
    x = 0,
    y = 0,
    z = 0,
}

local MoveComponent = class("MoveComponent",Component){
    speed = 200,
}

local MoveController = class("MoveController",Controller){
    move_component = nil,
    position_component = nil
}

function MoveController:_enter_entity(entity)
    self.move_component = entity:get_component(MoveComponent.__name__)
    self.position_component = entity:get_component(PositionComponent.__name__)
end

function MoveController:_update(dt)
    if self.move_component == nil or self.position_component == nil then
        return
    end
    local vx,vy = 0,0
    if love.keyboard.isDown("w") then
        vy = -self.move_component.speed
    end
    if love.keyboard.isDown("s") then
        vy = self.move_component.speed
    end
    if love.keyboard.isDown("a") then
        vx = -self.move_component.speed
    end
    if love.keyboard.isDown("d") then
        vx = self.move_component.speed
    end
    self.position_component.x = self.position_component.x + vx * dt
    self.position_component.y = self.position_component.y + vy * dt

    --local x,y = box_core.reader.camera:toCameraCoordsV3(self.position_component.x,self.position_component.y,self.position_component.z)
    self.entity.z_index = math.ceil(self.position_component.y)
end


local player = class("Player",Entity){}

function player:_ready()
    player:add_component(PositionComponent())
    player:add_component(MoveComponent())
    player:add_controller(MoveController())
end

function player:_render()
    local position = self:get_component(PositionComponent.__name__)
    box_core.reader:render_model(player_block_model,position.x,position.y,position.z)
end

function scene:_ready()
    local camera = box_core.reader.camera

    for x = 1,2 do
        for y = 10,15 do
            local b = block()
            b.x = x * 16
            b.y = y * 16
            b.z = 0
            --local x,y =  camera:toCameraCoordsV3(b.x,b.y,b.z)
            b.z_index = math.ceil(b.y)
            self:add_child(b)
        end
    end
    self:add_child(player)
    
    camera.scale = 1

    box_core.reader:foreach(function(node)
        print(node.z_index)
    end)
    camera.rotation = math.rad(25);
end

function scene:_update()
    local position = player:get_component(PositionComponent.__name__)

    --local x,y = box_core.reader.camera:toCameraCoordsV3(position.x,position.y,position.z)
    box_core.reader.camera:follow(position.x,position.y)
end

return scene
