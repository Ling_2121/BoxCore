require"box_core"

local debug = {}

function debug:draw()
    love.graphics.print(
        string.format([[
        FPS:%d
        DT:%f
        MEM:%f
        ]],
            love.timer.getFPS(),
            love.timer.getDelta(),
            collectgarbage("count")
        )
    )
end

function love.load()
    box_core:init_core()
    box_core.world:addEntity(box_core:create_entity("PlayerEntity"))
    box_core.callback:register("draw",debug,"draw",100)
end
