local sort_list = require"base.sort_list"

local callback = class("Callback"){
    callback_table = {
        draw = {},
        keypressed = {},
        keyreleased = {},
        wheelmoved = {},
        mousemoved = {},
        mousepressed = {},
        mousereleased = {},
        textinput = {},
        quit = {},
    }
}

function callback:__init__()
    for callback_name,table in pairs(self.callback_table) do
        self.callback_table[callback_name] = sort_list()
    end

    for callback_name,table in pairs(self.callback_table) do
        love[callback_name] = function(...)
            for node in table:items() do
                local callback = node.data
                callback.object[callback.func_name](callback.object,...)
            end
        end
    end
end

function callback:register(callback_name,object,func_name,priority)
    priority = priority or 0
    if self.callback_table[callback_name] then
        self.callback_table[callback_name]:add(priority,{
            object = object;
            func_name = func_name;
        })
    end
end

function callback:remove(callback_name,object)
    if self.callback_table[callback_name] then
        self.callback_table[callback_name]:remove(object)
    end
end

return callback