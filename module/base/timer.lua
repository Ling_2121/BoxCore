local Node = require"module.scene.node"

local timer = class("Timer",Node){
    timer = 0;
    time = 0;
}
:create_signal("complete")

function timer:set_time(t)
    self.time = t
end

function timer:reset()
    self.timer = 0
end

function timer:_complete()

end

function timer:_update(dt)
    self.timer = self.timer + dt
    if self.timer >= self.time then
        self.timer = 0
        self:emit_signal("complete")
        self:_complete()
    end
end

return timer