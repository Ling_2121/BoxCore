local vox_model = require"library.Lovox.lovox.vox2png.vox_model"
local vox_texture = require"library.Lovox.lovox.vox2png.vox_texture"

local model_data = class("ModelData"){
    frame_width = 0;
    frame_height = 0;
    frame_count = 0;
    quads = {};
    texture = nil;
}

function model_data:__init__(path)
    local file = love.filesystem.newFile(path)
    file:open("r")
        local model = vox_model.new(file:read())
    file:close()
    local texture = vox_texture.new(model)

    self.frame_width = texture.sizeX;
    self.frame_height = texture.sizeY;
    self.frame_count = texture.canvas:getWidth() / texture.sizeX;
    self.texture = texture.canvas;
    
    local tex_w,tex_h =  self.texture:getDimensions()
    for i = 0,self.frame_count - 1 do
        self.quads[i + 1] = love.graphics.newQuad(
            i * self.frame_width,
            0,
            self.frame_width,
            self.frame_height,
            tex_w,
            tex_h
        )
    end
end

return model_data