local RenderItem = require("module/render/render_objects/render_item")
local circle = class("CircleRenderItem",RenderItem){
    item_type = RENDER_ITEM_CIRCLE;
    fill_mode = FILL_MODE_FILL;
    radius = 0;
}

function circle:__init__(fill_mode,radius)
    self.fill_mode = fill_mode or FILL_MODE_FILL
    self.radius = radius or 10
end

function circle:draw(x,y)
    love.graphics.circle(self.fill_mode,x,y,self.radius)
end

return circle