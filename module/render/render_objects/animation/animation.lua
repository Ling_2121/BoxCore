local RenderProcessItem = require("module/render/render_objects/render_process_item")

local animation = class("AnimationRenderItem",RenderProcessItem){
    item_type = RENDER_ITEM_ANIMATION;
    play_speed = 0.5;
    frame = 1;
    frames = {};
    playing = true;
    _frame_count = 0;
    _timer = 0;
}

function animation:__init__(play_speed)
    self.play_speed = play_speed or 0.5
end

function animation:set_frame(i,frame_object)
    self.frames[i] = frame_object
    self._frame_count = #self.frames
    return self
end

function animation:add_frame(frame_object)
    table.insert(self.frames,frame_object);
    self._frame_count = #self.frames
    return self
end

function animation:set_speed(play_speed)
    self.play_speed = play_speed or 0.5
    return self
end

function animation:jump(frame)
    self.frame = math.min(math.max(1,frame),self._frame_count)
    return self
end

function animation:play(frame)
    self.playing = true;
    if frame then
        self:jump(frame)
    end
    return self
end

function animation:stop(frame)
    self.playing = false;
    if frame then
        self:jump(frame)
    end
    return self
end

function animation:process(dt)
    if self.playing then
        self._timer = self._timer + dt
        if self._timer >= self.play_speed then
            self._timer = 0
            self.frame = self.frame + 1
            if self.frame > self._frame_count then
                self.frame = 1
            end
        end
    end
end

return animation