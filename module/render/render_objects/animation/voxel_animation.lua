local Animation = require"module/render/render_objects/animation/animation"

local voxel_animation = class("VoxelAnimationRenderItem",Animation){}

function voxel_animation:draw(x,y,z,r,xs,ys)
    local frame = self.frames[self.frame]
    if frame then
        frame:draw(x,y,z,r,xs,ys)
    end
end

return voxel_animation