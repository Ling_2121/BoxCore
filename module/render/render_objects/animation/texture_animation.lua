local Animation = require"module/render/render_objects/animation/animation"

local texture_animation = class("TextureAnimationRenderItem",Animation){}

function texture_animation:draw(x,y,r,xs,ys)
    local frame = self.frames[self.frame]
    if frame then
        frame:draw(x,y,r,xs,ys)
    end
end

return texture_animation