local RenderItem = require("module/render/render_objects/render_item")

local rectangle = class("RectangleRenderItem",RenderItem){
    item_type = RENDER_ITEM_RECTANGLE;
    fill_mode = FILL_MODE_FILL;
    width = 0;
    height = 0;
}

function rectangle:__init__(fill_mode,width,height)
    self.fill_mode = fill_mode or FILL_MODE_FILL
    self.width = width or 10
    self.height = height or 10
end

function rectangle:draw(x,y)
    love.graphics.rectangle(self.fill_mode,x,y,self.width,self.height)
end

return rectangle