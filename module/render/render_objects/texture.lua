local RenderItem = require("module/render/render_objects/render_item")

local texture = class("CircleRenderItem",RenderItem){
    item_type = RENDER_ITEM_TEXTURE;
    texture_path = "",
    texture = nil;
}

function texture:__init__(texture_path)
    if type(texture) == "string" then
        self.texture_path = texture_path
        self.texture = box_core.assets.texture(texture_path)
    else
        self.texture = texture_path
    end
end

function texture:draw(x,y,...)
    love.graphics.draw(self.texture,x,y,...)
end

return texture