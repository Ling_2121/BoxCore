local renderer = class("Renderer"){
    render_mode = RENDER_MODE_DEFAULT
}

function renderer:__init__(config)
    config = config or {}
    self.render_mode = config.render_mode or RENDER_MODE_DEFAULT
    self._render = config._render or renderer._render
end

function renderer:_render(entity,render_system)

end

return renderer